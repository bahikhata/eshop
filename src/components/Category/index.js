import React from 'react';
import './index.css';
import {  Button, Container, Row, Col, Image, Form, Card } from 'react-bootstrap';
import { withTranslation } from 'react-i18next';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

class Index extends React.Component {
  constructor(props){
    super(props);
    this.state = {

    }
  }

  componentDidMount(){

  }

  componentWillUnmount(){

  }

  handleClick = () => {
    alert('hello');
  }

  render(){
    const {t, i18n, name} = this.props;
    return (
      <Card style={{ width: '10rem'}}>
            <Card.Img variant="top" src="http://www.placehold.it/100x100" />
            <Card.Body>
              <Card.Title>{name}</Card.Title>
            </Card.Body>
      </Card>
    );
  }
}

export default withTranslation()(Index);
