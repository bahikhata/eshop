import React from 'react';
import './index.css';
import {  Button, Container, Row, Col, Image, Form, Card } from 'react-bootstrap';
import { withTranslation } from 'react-i18next';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

class Index extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      name: "Abhijeet",
      date: new Date()
    }
  }

  componentDidMount(){
    this.setState({name: "Goel"});
  }

  componentWillUnmount(){

  }

  handleClick = () => {
    alert('hello');
  }

  render(){
    const {t, i18n} = this.props;
    return (
      <Container>
        <Row>
          <Col>
            <Form className="login-form">
              <Form.Group controlId="formEmail">
                <Form.Control type="text" placeholder="Email"/>
              </Form.Group>
              <Form.Group controlId="formPassword">
                <Form.Control type="password" placeholder="Password"/>
              </Form.Group>
              <Button variant="primary" type="submit">
                Log In
              </Button>
              <Button variant="primary" type="submit">
                Reset
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default withTranslation()(Index);
