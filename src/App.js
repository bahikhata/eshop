import React from 'react';
import logo from './logo.svg';
import './App.css';
import {  Button, Container, Row, Col, Image, Form, Card } from 'react-bootstrap';
import { withTranslation } from 'react-i18next';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import Home from './views/Home';
import ProductDetail from './views/ProductDetail';
import CatDetail from './views/CatDetail';
import Cart from './views/Cart';
import Checkout from './views/Checkout';
import Orders from './views/Orders';

class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {

    }
  }

  render(){
    const {t, i18n} = this.props;
    return (
      <Router>
          <Route exact path="/" component={Home} />
          <Route path="/category" component={CatDetail} />
          <Route path="/product" component={ProductDetail} />
          <Route path="/cart" component={Cart} />
          <Route path="/checkout" component={Checkout} />
          <Route path="/orders" component={Orders} />
      </Router>
    );
  }

}

export default App;
