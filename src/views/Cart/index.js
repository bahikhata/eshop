import React from 'react';
//import logo from '../../images/logo.svg';
import './index.css';
import { withTranslation } from 'react-i18next';
import {  Button, Container, Row, Col, Image,
  Jumbotron, Carousel, Dropdown, Navbar, Nav, ListGroup
} from 'react-bootstrap';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Login from '../../components/Login';

//import api from '../../api';
//const { db, mock } = api;

class Index extends React.Component {
  constructor(props){
    super(props);
    this.state = {

    }
  }

  componentDidMount(){
    //console.log(mock.getData());
  }

  componentWillUnmount(){

  }

  render(){
    const {t, i18n} = this.props;
    return (
      <Container>
        <Navbar expand="lg" fixed="bottom" >
          <Container>
            <Nav className="mr-auto">
              <Nav.Link href="/">Home</Nav.Link>
              <Nav.Link href="/categories">Categories</Nav.Link>
              <Nav.Link href="/Cart">Bag</Nav.Link>
            </Nav>
          </Container>
        </Navbar>
        <Row>

          <Col md={6}>
            <h3>Cart ({})</h3>
            <ListGroup>
            <ListGroup.Item>
              <Row>
                <Col md={4}>
                  <Image src="http://placehold.it/100X100"></Image>
                </Col>
                <Col md={8}>
                  <h4>Title</h4>
                  <p>Description</p>
                </Col>
              </Row>
            </ListGroup.Item>
            <ListGroup.Item>Dapibus ac facilisis in</ListGroup.Item>
            <ListGroup.Item>Morbi leo risus</ListGroup.Item>
            <ListGroup.Item>Porta ac consectetur ac</ListGroup.Item>
            <ListGroup.Item>Vestibulum at eros</ListGroup.Item>
          </ListGroup>

          </Col>
          <Col md={6}>
              
              <Link to="/checkout"><Button block>Select Address</Button></Link>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default withTranslation()(Index);
