import React from 'react';
//import logo from '../../images/logo.svg';
import './index.css';
import { withTranslation } from 'react-i18next';
import {  Button, Container, Row, Col, Image, Jumbotron, Carousel, Dropdown } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLongArrowAltLeft } from '@fortawesome/free-solid-svg-icons'

//import api from '../../api';
//const { db, mock } = api;

class Index extends React.Component {
  constructor(props){
    super(props);
    this.state = {

    }
  }

  componentDidMount(){
    //console.log(mock.getData());
    const { product } = this.props;
    fetch('http://localhost:3030/product/' + product.id)
      .then(res => res.json())
      .then((data) => {
        this.setState({
          product: data
        });
      });
  }

  componentWillUnmount(){

  }

  render(){
    const {t, i18n, product } = this.props;
    if(!product){
      return null;
    }
    return (
      <Container>
        <Row>
          <Col md={1}>
            <Link to="/">
              <FontAwesomeIcon icon={faLongArrowAltLeft} size="4x" color="#000"/>
            </Link>
          </Col>
          <Col md={11}>
            <h3>{product.detail}</h3>
          </Col>
        </Row>
        <Row>
          <Col md={5}>
            <br />
            <Image src="http://placehold.it/400X400"></Image>
          </Col>
          <Col md={7}>
            <p>{product.name}</p>
            <p>{product.description}</p>
            <p>Quantity</p>
            <p>MRP</p>
            <p>Sale Price</p>
            <br />
            <Row>
              <Col md={6}><Link to="/cart"><Button variant="outline-primary" block>Add to Bag</Button></Link></Col>
              <Col md={6}><Link to="/cart"><Button block>Buy Now</Button></Link></Col>
            </Row>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default withTranslation()(Index);
