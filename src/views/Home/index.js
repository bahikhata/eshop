import React from 'react';
//import logo from '../../images/logo.svg';
import './index.css';
import { withTranslation } from 'react-i18next';
import {  Button, Container, Row, Col, Image, Jumbotron, Carousel, Dropdown } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Login from '../../components/Login';
import Category from '../../components/Category';
import Product from '../../components/Product';
//import api from '../../api';
//const { db, mock } = api;

class Index extends React.Component {
  constructor(props){
    super(props);
    this.state = {

    }
  }

  componentDidMount(){
    //console.log(mock.getData());
    //console.log(api);
    fetch('http://localhost:3030/product')
      .then(res => res.json())
      .then((data) => {
        this.setState({
          products: data
        });
      });
  }

  componentWillUnmount(){

  }

  render(){
    const {t, i18n} = this.props;
    const { products } = this.state;
    if(!products) {
      return null;
    }
    const productsHtml = products.map((product, i) => {
      const link = "/product/" + product.id;
      return (
        <Col md={3} key={i}>
          <Link to={link}>
            <Product product={product} />
          </Link>
        </Col>
      )
    });
    return (
      <Container>
        <Row>
          <Col md={2}>
            <Image src="http://placehold.it/100X100"></Image>
          </Col>
          <Col md={10}>
            City Mall
          </Col>
        </Row>
        <br />
        <Row>
          {productsHtml}
        </Row>
      </Container>
    );
  }
}

export default withTranslation()(Index);
