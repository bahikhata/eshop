import React from 'react';
//import logo from '../../images/logo.svg';
import './index.css';
import { withTranslation } from 'react-i18next';
import {  Button, Container, Row, Col, Image, Jumbotron, Carousel, Dropdown, Form } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Login from '../../components/Login';

//import api from '../../api';
//const { db, mock } = api;

class Index extends React.Component {
  constructor(props){
    super(props);
    this.state = {

    }
  }

  componentDidMount(){
    //console.log(mock.getData());
  }

  componentWillUnmount(){

  }

  render(){
    const {t, i18n} = this.props;
    return (
      <Container>
        <Row>
          <Col md={12}>
            Checkout
            <Form>
                  <Form.Control type="text" plaintext placeholder="Name" />
                  <Form.Control type="text" plaintext placeholder="Address" />
              <Row>
                <Col>
                  <Form.Control type="text" plaintext placeholder="City" />
                </Col>
                <Col>
                  <Form.Control type="text" plaintext placeholder="Pincode" />
                </Col>
              </Row>
            </Form>
            <br /><br />
            Payment Method
            <Form.Check
              type="checkbox"
              id='checkbox'
              label="Pay on Delivery"
              checked
            />
            <br />
            <Row>
              <Col>
                <Link to="/status"><Button size="lg" block>Place Order</Button></Link>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default withTranslation()(Index);
